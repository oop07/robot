/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.robot;

/**
 *
 * @author nonnanee
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(0,0,6,2,100);
        System.out.println(robot);
        robot.walkToDirectionOneStep('N');
        System.out.println(robot);
        robot.walkToDirectionOneStep('S');
        robot.walkToDirectionOneStep('S');
        System.out.println(robot);
        robot.walkToDirectionNStep('E',2);
        System.out.println(robot);
        robot.walkOneStep();
        System.out.println(robot);
        robot.walkNStep(3);
        System.out.println(robot);
      
    }
}
